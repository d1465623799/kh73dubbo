package com.kgc.cn.utils.activeMqUtil;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Queue;

/**
 * @auther zhouxinyu
 * @data 2019/12/14
 */
@Component
public class ActiveMqUtil {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    /**
     * 发邮件消息中间件
     *
     * @param name
     * @param o
     */
    public void sendEmailByActiveMq(String name, Object o) {
        Queue queue = new ActiveMQQueue(name);
        jmsMessagingTemplate.convertAndSend(queue, o);

    }

}
