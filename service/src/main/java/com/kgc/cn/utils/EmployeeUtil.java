package com.kgc.cn.utils;

import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * @data 2019/12/13
 */
@Component
public class EmployeeUtil {

    /**
     * 生成员工号（手机号后四位+身份证生日+随机四位数）
     *
     * @param phone
     * @param UId
     * @return
     */
    public String generateEmpId(String phone, String UId) {
        return phone.substring(7, 11) + UId.substring(6, 14) + generateFour();
    }

    /**
     * 生成随机四位数
     *
     * @return
     */
    public String generateFour() {
        int num = 0;
        do {
            Random random = new Random();
            num = random.nextInt(10000);
        } while (num < 1000);
        return num + "";
    }
}
